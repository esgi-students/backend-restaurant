var express = require('express');
var app = express();
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/restaurant');

var router = express.Router();


router.use(bodyParser.urlencoded({ extended: true }));


app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

app.use(morgan('dev'));


var Schema = mongoose.Schema;

var restaurant = new Schema({
    name:{
        type: String,
        required: true,
        unique:true
    },
    description:{
        type:String,
        required: false
    },
    address:{
        type: String,
        required: true
    },
    country:{
        type:String,
        required: false
    },
    author:{
        type:String,
        required: false
    },
    loc: {
        type: [Number],  // [<longitude>, <latitude>]
        index: '2s'      // create the geospatial index
    }

});

var Restaurant = mongoose.model("Restaurant",restaurant);

var array = []


router.post('/restaurants', function (req,res,next) {


    console.log(req.body.loc[0])

    var coords = []
    coords[0] = Number(req.body.loc[0]);
    coords[1] = Number(req.body.loc[1]);


    rest = new Restaurant({
        name: req.body.name,
        description: req.body.description,
        address: req.body.address,
        country: req.body.country,
        loc: coords

    });

    rest.save(function (err, instance) {
        if(err){
            console.log(err);
            return res.status(500).send(err);
        }


        return res.status(200).send(instance)
    });


});


router.delete('/restaurants', function (req,res,next) {

    var id = req.body["id_host"];
    console.log(id);

    Restaurant.findByIdAndRemove(id, function (err, instance) {
        if(err){
            return res.status(500).send(err)
        }
        return res.status(200).send(instance)
    })

});


router.put('/restaurants', function (req,res,next) {

    var id = req.body["id_host"];

    var coords = []
    coords[0] = parseFloat(req.body.loc[0]);
    coords[1] = parseFloat(req.body.loc[1]);

    var rest = {
        name: req.body.name,
        description: req.body.description,
        address: req.body.address,
        loc: coords

    };

    if(typeof req.body.country == "undefined") {
        console.log("WRONG")
    }else{
        rest["country"] = req.body.country
    }

    console.log(rest)



    Restaurant.findByIdAndUpdate(id, rest, function (err, instance) {
        if(err){
            console.log(err)
            return res.status(500).send(err)
        }
        return res.status(200).send(instance)
    });

});

router.get('/restaurants', function (req,res,next) {

    var query = Restaurant.find()

    query.exec(function (err, instances) {
        if (err)
            return res.status(500).send(err);
        res.send(instances);
    })
})


app.use('/paris',router)


app.listen(app.get('port'), function() {
    console.log('Node app is running on port', app.get('port'));
});
